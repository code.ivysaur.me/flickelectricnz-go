# flickelectricnz-go

![](https://img.shields.io/badge/written%20in-golang-blue)

A library to retrieve pricing information from the NZ electricity retailer "Flick".

- Ported from the node.js / coffeescript package https://github.com/madleech/FlickElectricApi by @madleech (ISC license).
- Uses synchronous requests, allowing callers to wrap in goroutines as appropriate.
- Includes a sample command-line tool to retrieve prices.

## Changelog

2017-06-10 1.1
- Compatibility update
- Update package import path
- [⬇️ flickelectricnz-1.1-win64.7z](dist-archive/flickelectricnz-1.1-win64.7z) *(903.50 KiB)*
- [⬇️ flickelectricnz-1.1-src.zip](dist-archive/flickelectricnz-1.1-src.zip) *(8.50 KiB)*
- [⬇️ flickelectricnz-1.1-linux64.tar.xz](dist-archive/flickelectricnz-1.1-linux64.tar.xz) *(997.13 KiB)*


2016-08-24 r1
- Initial release
- [⬇️ get-flick-price-r1-win64.7z](dist-archive/get-flick-price-r1-win64.7z) *(871.59 KiB)*
- [⬇️ get-flick-price-r1-win32.7z](dist-archive/get-flick-price-r1-win32.7z) *(796.87 KiB)*
- [⬇️ get-flick-price-r1-linux64.tar.xz](dist-archive/get-flick-price-r1-linux64.tar.xz) *(982.87 KiB)*
- [⬇️ get-flick-price-r1-linux32.tar.xz](dist-archive/get-flick-price-r1-linux32.tar.xz) *(921.59 KiB)*
- [⬇️ flickelectricnz-r1-src.7z](dist-archive/flickelectricnz-r1-src.7z) *(1.98 KiB)*

